import React, { useEffect, useState } from "react";
import ListMessages from "./ListMessages";
import horizontal_line from "./images/horizontal-line.png";

const Messages = () => {
    const URL = "https://w81db-f4a7e-default-rtdb.europe-west1.firebasedatabase.app/";
    const FILE = "test.json";
    const [messages, setMessages] = useState({});
    const [milliseconds, setMilliseconds] = useState(950); // We have to have some delay in loading messages
                                                             // 'cause in firebase there's some lag in saving message
                                                             // and after pressing submit the message wouldn't be shown
                                                             // when "read messages" page was loaded

    //Fetch data from Firebase
    useEffect(() => {
        let interval = null;
        if (milliseconds === 1000) {
            fetch(URL + FILE)
                .then(function(response) {
            if (response.status !== 200) {
                console.log("Looks like there was a prublemings! Status code: " + response.status);
                return;
            }
            response.json().then(function(data) {
                setMessages(data);
                setMilliseconds(0);
            });
            })
            .catch(function(err) {
            console.log("Fetch error: ", err);
        });
        } else {
            interval = setInterval(() => {
                setMilliseconds(milliseconds => milliseconds + 1);
            }, 1);
            //console.log(milliseconds); // remove before release
        }

        return () => clearInterval(interval);
        
    },[ milliseconds]);

    const messageArray = Array.from(Object.keys(messages), k => messages[k]);

    return(
        <div>
            <h2>MESSAGES</h2>
            <img src={horizontal_line} />
            <ListMessages list={messageArray} />
        </div>
    )
}

export default Messages;