// Here we convert the msg date from UNIX Epoch to human readable format

const DatePrinter = (props) => {
    let msgDate = new Date(props.time * 1);

    return (msgDate.toLocaleString());
}

export default DatePrinter;