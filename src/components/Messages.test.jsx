import React from "react";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { configure, shallow } from "enzyme";
import Messages from "./Messages";

configure({adapter: new Adapter()});

describe("Messages component", () => {
    it("Test exists", () => {
        const wrapper = shallow(<Messages />);
        expect(wrapper.find("h2").html()).toEqual("<h2>MESSAGES</h2>");
    });
    
});