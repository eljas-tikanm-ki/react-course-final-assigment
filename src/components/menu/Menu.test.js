import React from "react";
import { configure, shallow, assert } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Menu from "./Menu";
configure({ adapter: new Adapter() });
describe('Testing Menu-component")', () => {
  it("There is one div", () => {
    const wrapper = shallow(<Menu />);
    expect(wrapper.find("div")).toHaveLength(1);
  });
});
