import React from "react";
import { Link } from "react-router-dom";
import "./Menu.css";

const Menu = () => {
    return (
        <div id="MenuDiv">
            <table>
                <tr>
                    <td><Link to="/">Read messages</Link></td>
                    <td><Link to="/write">Write message</Link></td>
                    <td><Link to="/about">About this software</Link></td>
                </tr>
            </table>
        </div>
    );
}

export default Menu;