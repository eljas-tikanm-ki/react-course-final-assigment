import React from "react";
import DatePrinter from "./DatePrinter";
import { configure, shallow, assert } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });
describe('Date printer test', () => {
  
  it("Epoch 1 returns 1/1/1970", () => {
    const wrapper = shallow(<DatePrinter time="1" />);
    expect(wrapper.text("01/01/1970, 2.00.00"));
  });
});
