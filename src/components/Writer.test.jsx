import React from "react";
import Write from "./Write";
import {MemoryRouter} from 'react-router-dom'
import { render, screen } from "@testing-library/react";

test('Write component should show form', () => {
    render(<Write />, {wrapper: MemoryRouter})
    //screen.debug();
    expect(screen.getByText("Name:"));
})