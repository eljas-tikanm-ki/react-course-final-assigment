import "./css/ListMessages.css";
import horizontal_line from "./images/horizontal-line.png";
import DatePrinter from "./DatePrinter";

const ListMessages = (props) => {
    // First lets reverse the list for "newest msg first" action
    props.list.reverse();

    return( //Lets go through the list of messages and print 'em out
        <div>
        {props.list.map((message) => {
            
            return( 
                <div>
                    <div className="leftDiv">{message.author} <br/><DatePrinter time={message.time} /></div>
                    <div className="rightDiv">{message.msg}</div>
                    <img  src={horizontal_line} />
                </div>);
        })}
        </div>
    );
}

export default ListMessages;