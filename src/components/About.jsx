import "./css/About.css";
import pic from "./images/logo.svg";
import horizontal_line from "./images/horizontal-line.png";

const About = () => {
    return (
        <div className="About">
            <h2>ABOUT THIS SOFTWARE</h2>
            <img src={horizontal_line} /><br />
            <img src={pic} className="React-logo" />
            <p>Made with react!</p>

            <img src={horizontal_line} />

            <p className="Left">This piece of software is not meant to be finished product in any way, only a showcase my current React skills gained in winter 2021-2022.</p>

            <p className="Left">Source code can be found at <a href="https://gitlab.com/eljas-tikanm-ki/w81" target="_blank"><b>gitlab.com/eljas-tikanm-ki/w81</b></a></p>

            <p className="Left">Application utilize 6 components in addition to index.js and App.js, using props to pass data between them, for example, Message sends the message-array to MessageList -component which map and renders the messages. The application is implemented in the 2021 way and have unit testing. Software uses React Routes, it connects to JSON file in Firebase to save and retrieve data, simple form validation, "Read Messages" component refreshes messages every 2 seconds.</p>
            
            <p className="Left"><b>Major bugs</b> have been found. Here's couple:</p>
            
            <ul  className="Left">
            <li>With an empty database (= empty JSON file) the entire app will render nothing</li>
            <li>If two users go on writing a message at same time, they will have same message id. The one who sends message later will overwrite the message of first poster</li>
            </ul>
            <p className="Left">These are issue because lack of proper backend database management because using dumb system (JSON file at remote server). There's two ways for that: 1) use php+MySql webserver for message management or 2) build the message management system in App. But in this case there is no need for backend.</p>

            <img src={horizontal_line} />
        </div>
    );
}

export default About;