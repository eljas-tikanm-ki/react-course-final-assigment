import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./css/Write.css";
import horizontal_line from "./images/horizontal-line.png";

const Write = () => {
    const URL = "https://w81db-f4a7e-default-rtdb.europe-west1.firebasedatabase.app/";
    const FILE = "test.json";
    const [messages, setMessages] = useState({});
    const [author, setAuthor] = useState();
    const [msg, setMsg] = useState();
    const navigate = useNavigate();

    //While loading page, lets have all the messages for this new messages id
   useEffect(() => {
    fetch(URL + FILE)
    .then(function(response) {
        if (response.status !== 200) {
            console.log("Looks like there was a prublemings! Status code: " + response.status);
            return;
        }
        response.json().then(function(data) {
            setMessages(data);
        });
    })
    .catch(function(err) {
        console.log("Fetch error: ", err);
    });
    },[]);

    return ( // Letting out the new msg form
        <div className="writeDiv">
          <h2>WRITE A MESSAGE</h2>
          <img src={horizontal_line} />
          <p id="error"></p>
          <form>
            <table>
              <tr>
                <td className="writeTd">Name:</td><td className="writeTd"><input type="text" id="author" onChange={e => setAuthor(e.target.value)} onBlur={validate1}/></td>
              </tr>
              <tr>
                <td className="writeTd">Message:</td><td className="writeTd"><textarea rows={5} id="textarea" onChange={e => setMsg(e.target.value)} onBlur={validate2}/></td>
              </tr>
            </table>
              <button onClick={SaveMessage}>Send message</button>          
          </form>
          <img src={horizontal_line} />
        </div>
    );

    //-----------------------------FUNCTIONS TO VALIDATE FORM --------------------------------
    function validate1() {
      if(document.getElementById("author").value.length < 1) {
        document.getElementById("author").style.background = "#ff9191";
      } else {
        document.getElementById("author").style.background = "white";
      }
    }

    function validate2() {
      if(document.getElementById("textarea").value.length < 1) {
        document.getElementById("textarea").style.background = "#ff9191";
      } else {
        document.getElementById("textarea").style.background = "white";
      }
    }

    //-----------------------------FUNCTION TO SAVE NEW MESSAGE --------------------------------
    function SaveMessage() {      
      // Check if empty fields are present, if so function will quit
      if(document.getElementById("author").value.length < 1 || document.getElementById("textarea").value.length < 1) {
        document.getElementById("error").innerHTML = "There cannot be empty fields!";
        
        return;
      }
      // Here we create our JSON string to put in firebase
      const msgArr = Array.from(Object.keys(messages)); //First get our message id for new message
      const mid = msgArr.length; // Houston we got the mid

      // Timestamp for message as unix epoch
      const d = new Date();
      let time = d.getTime();

      // Construct the command!
      let json_message = "{ \"" + mid + "\": { \"author\": \"" + author + "\", \"time\": \"" + time + "\", \"msg\": \"" + msg + "\" }}";
      console.log(json_message);// remove before release

        // Here we PATCH new message in firebase json file
        fetch(URL + FILE, {method: "PATCH", "Content-Type": "application/json", body: json_message})
            .then(function(response) {
              //response is plain encoded text
              if (response.status !== 200) {
                console.log("Looks like there was a problem. Status Code: " + response.status);
                return;
              }
              //convert text to json
              response.json().then(function(data) {
                console.log(data);
              });
            })
            .catch(function(err) {
              console.log("Fetch Error : ", err);
        });
        navigate("/"); // automated return to "read messages"
    }
}

export default Write;