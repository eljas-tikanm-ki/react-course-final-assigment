import './App.css';
import { Route, Routes } from "react-router-dom";
import Menu from './components/menu/Menu';
import Messages from './components/Messages';
import Write from './components/Write';
import About from './components/About';


function App() {
  return (
    <div className="App">
      <Menu />
      <Routes>
        <Route path="/" element={<Messages />} />
        <Route path="/write" element={<Write />} />
        <Route path="/about" element={<About />} />
      </Routes>
    </div>
  );
}

export default App;