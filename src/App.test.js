import {render, screen} from '@testing-library/react'
import React from 'react'
import '@testing-library/jest-dom'
import App from './App'

import {MemoryRouter} from 'react-router-dom'

test('full app rendering', () => {
  render(<App />, {wrapper: MemoryRouter})
  expect(screen.getByText("MESSAGES")).toBeInTheDocument()
})